# Teste Desenvolvedor iOS BRQ #

### Objetivo ###

Esse teste tem como objetivo avaliar os conhecimentos de desenvolvimento para iOS, levando em conta:

* Conhecimentos da linguagem utilizada
* Arquitetura do projeto
* Boas práticas
* Utilização do Git

### Requisitos ###

* Desenvolver o projeto em Objective-c ou Swift
* Utilizar Xcode 8 ou superior
* Utilizar no máximo 2 pods e/ou biblotecas de terceiros
* Seguir os padrões do Git

### Projeto ###

* Criar um aplicativo que contenha pelo menos duas telas.
* A primeira deve buscar a localização do usuário através do GPS e mostrar a previsão do tempo
* A segunda deve mostrar a previsão do tempo em Nova York
* Utilizar a API https://openweathermap.org/api

### Diferenciais ###

* Tratamento para falta de conexão com a internet
* Testes unitários e de interface

### Conclusão ###

* Ao finalizar, criar um pull request para este repositório para que o mesmo possa ser avaliado posteriormente
* Se desejar, faça qualquer justificativa na mensagem do seu pull request